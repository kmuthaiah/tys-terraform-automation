resource "aws_instance" "peer_node" {
    ami = "${lookup(var.ami, var.aws_region)}"
    instance_type = "t2.micro"
    subnet_id = "${aws_subnet.prod-subnet-public-1.id}"
    vpc_security_group_ids = ["${aws_security_group.ssh-allowed.id}"]
    key_name = "${aws_key_pair.us-region-key-pair.id}"
    connection {
        user = "${var.ec2_user}"
        private_key = "${file("${var.private_key_path}")}"
        host = "self.public_ip"
    }

    tags = {
        Name = "tys-peer-node"
    }
}
// Sends your public key to the instance
resource "aws_key_pair" "us-region-key-pair" {
    key_name = "us-region-key-pair.pub"
    public_key = "${file(var.public_key_path)}"
}
