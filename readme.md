ec2_instance.tf
===============
This terraform script is used to create the Peer Node EC2 instance.

bastion.tf
==========
This terraform script is used to create the bastion host

vpc.tf
======
This terraform script is used to create the VPC in the AWS cloud.

variables.tf
============
This terraform script holds all the variables that is used in the other terraform scripts.

provider.tf
===========
This terraform script is used to mention which service provider will be used for creation of the different infrastructure components.

network.tf
==========
This terraform script is used to create the security group, route table association and internet gateways.
