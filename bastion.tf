resource "aws_instance" "bastion_host" {
    ami = "${lookup(var.ami, var.aws_region)}"
    instance_type = "t2.micro"
    subnet_id = "${aws_subnet.prod-subnet-public-1.id}"
    vpc_security_group_ids = ["${aws_security_group.bastion-sg.id}"]
    key_name = "${aws_key_pair.bastion_key.id}"
    connection {
        user = "${var.ec2_user}"
        private_key = "${file("${var.private_key_path}")}"
        host = "self.public_ip"
    }

    tags = {
        Name = "tys-bastion-host"
    }
}



resource "aws_security_group" "bastion-sg" {
  name   = "bastion-security-group"
  vpc_id = "${aws_vpc.tys-prod-vpc.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0 
    to_port     = 0 
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
        Name = "bastion host"
  }

}

resource "aws_key_pair" "bastion_key" {
  key_name   = "/home/kdharuman/.ssh/id_rsa.pub"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLvWW/+bRjds8YkO05/p0DvgYfz7eu9VyF8TSH15bB1PTO+kEPxFAh/gwHuJOvqIa/sSDgUKsmIDAIpFFELqEQIjlqEox6n33rt7XFz3UG2YafEFnMIY4PEn8O5c6FUS0WqOu7zzL4ZB6nqZ5AAiE9Z9LQnCtVVzCzYfNd5aLfZCd+tkf3hz3v/yePb/jOFAXIZ9ahb9R7GRkEhKTfl3UABltnscBzpm6Ra6NcxQiKwDMeUPRei33ehgOSETY+C9l3Y33pa0J+4PUeclWL0eSltzebOF97+WrmBXA/sCtIuvfRw67BZrIsrCJ/AM0yVSdyRUZI2zaxiRltScIENmALjj3xdMRSEMlB8k1mTKjXUvo3Z4lnE7yawdMAmaih7whXV2gIwIXSy8nQRvFXOFQuVfoSHwTaIa005dvbYnl4ksZ1yzg/b25sejcH8laCDkTU53494F62fAz4jNFUZ96sDZkpR+694QmGlAca3/dX4cptlJWCKmGckSw3R8OHlZM= kdharuman@ubuntu"
}

//output "bastion_public_ip" {
//  value = "${aws_instance.bastion.public_ip}"
//}
