variable "aws_region" {    
    default = "ap-south-1"
}

variable "ami" {
    type = map(string)
    
    default = {
	ap-south-1="ami-0c1a7f89451184c8b"
    }
}

variable "public_key_path" {
	default = "/home/kdharuman/terraform_tys/us-region-key-pair.pub"
}

variable "private_key_path" {
	default = "/home/kdharuman/terraform_tys/us-region-key-pair"
}

variable "ec2_user" {
	default = "ubuntu"
}
